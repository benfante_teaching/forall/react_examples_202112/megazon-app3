/**
 *
 * Counters
 *
 */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/macro';
import { useCountersSlice } from './slice';
import { selectCounters } from './slice/selectors';

interface Props {}

export function Counters(props: Props) {
  const { actions } = useCountersSlice();
  const dispatch = useDispatch();
  const counters = useSelector(selectCounters);

  const incrementCounter = (id: number) => {
    dispatch(actions.incrementCounter(id));
  };

  const decrementCounter = (id: number) => {
    dispatch(actions.decrementCounter(id));
  };

  useEffect(() => {
    dispatch(actions.startLoadCounters());
  }, [actions, dispatch]);

  return (
    <Div>
      <ul>
        {counters.map(counter => (
          <li key={counter.id}>
            Counter {counter.id}: {counter.value}
            <button onClick={() => decrementCounter(counter.id)}>-</button>
            <button onClick={() => incrementCounter(counter.id)}>-</button>
          </li>
        ))}
      </ul>
      <button onClick={() => dispatch(actions.addCounter())}>
        Add Counter
      </button>
    </Div>
  );
}

const Div = styled.div``;
