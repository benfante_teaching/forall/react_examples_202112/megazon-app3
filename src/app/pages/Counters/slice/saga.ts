import { PayloadAction } from '@reduxjs/toolkit';
import {
  take,
  call,
  put,
  select,
  takeLatest,
  takeEvery,
} from 'redux-saga/effects';
import { request } from 'utils/request';
import { countersActions as actions } from '.';
import { selectCounters } from './selectors';
import { Counter } from './types';

// function* doSomething() {}

function* loadCounters() {
  let counters: Counter[] = yield call(
    request,
    'http://localhost:3001/counters',
  );
  yield put(actions.loadCountersDone(counters));
}

function* updateCounter(action: PayloadAction<number>) {
  const counters: Counter[] = yield select(selectCounters);
  const counter = counters.find(counter => counter.id === action.payload);
  yield call(request, `http://localhost:3001/counters/${counter!.id}`, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify(counter),
  });
}

function* addCounter() {
  yield call(request, `http://localhost:3001/counters`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify({ value: 0 }),
  });
  //  yield put(actions.startLoadCounters);
  yield call(loadCounters);
}

export function* countersSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
  yield takeLatest(actions.startLoadCounters.type, loadCounters);
  yield takeEvery(actions.incrementCounter.type, updateCounter);
  yield takeEvery(actions.decrementCounter.type, updateCounter);
  yield takeEvery(actions.addCounter.type, addCounter);
}
