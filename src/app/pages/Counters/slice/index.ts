import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { countersSaga } from './saga';
import { CountersState, Counter } from './types';

export const initialState: CountersState = {
  counters: [],
};

const slice = createSlice({
  name: 'counters',
  initialState,
  reducers: {
    incrementCounter(state, action: PayloadAction<number>) {
      const counter = state.counters.filter(
        counter => counter.id === action.payload,
      )[0];
      counter.value = counter.value + 1;
    },
    decrementCounter(state, action: PayloadAction<number>) {
      const counter = state.counters.filter(
        counter => counter.id === action.payload,
      )[0];
      counter.value = counter.value - 1;
    },
    addCounter(state) {},
    startLoadCounters(state) {
      state.counters = [];
    },
    loadCountersDone(state, action: PayloadAction<Counter[]>) {
      state.counters = action.payload;
    },
  },
});

export const { actions: countersActions } = slice;

export const useCountersSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: countersSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useCountersSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
